<?php

/**
 * Defines the SDS Helper API Controller.
 */
class MysiteApiController extends HfcGlobalBaseContentApi {

  /**
   * Returns index for specified content type.
   *
   * @param string $type
   *   The content type to select.
   */
  public static function index(string $type): void {

    self::checkApiKey();

    // Disable caching for this callback.
    $GLOBALS['conf']['cache'] = 0;

    $result = db_query(
      "SELECT nid, uuid, changed FROM {node} WHERE status = 1 AND type = :type",
      [':type' => $type]
    )->fetchAllAssoc('nid');
    drupal_json_output($result);
    drupal_exit();
  }

  /**
   * Returns a node.
   */
  public static function content(object $node): void {

    self::checkApiKey();

    unset($node->data);

    // Convert all file URIs.
    foreach ([
      'field_artist_resume',
      'field_artwork_image',
      'field_attachment',
      'field_attachments',
      'field_button_image',
      'field_exhibition_images',
      'field_gallery_image',
      'field_header_image',
    ] as $fieldname) {
      if (!empty($node->{$fieldname})) {
        self::convertFileUri($node->{$fieldname});
      }
    }

    $node->url = '/' . drupal_get_path_alias("node/{$node->nid}");

    drupal_json_output($node);
    drupal_exit();
  }

  /**
   * Returns all tags.
   */
  public static function tags(): void {
    // Disable caching for this callback.
    $GLOBALS['conf']['cache'] = 0;
    $result = db_query("SELECT tid FROM {taxonomy_term_data}")->fetchCol();
    $terms = taxonomy_term_load_multiple($result);
    drupal_json_output($terms);
  }

}
