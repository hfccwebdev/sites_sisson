<?php

/**
 * Defines the Header Image Block.
 */
class MysiteHeaderImageBlock extends HfcGlobalBaseBlock {

  /**
   * {@inheritdoc}
   */
  public function info() {
    return [
      'info' => t('Display Header Image Content'),
      'cache' => DRUPAL_NO_CACHE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function build(&$output) {
    $placeholder = variable_get('mysite_display_header_placeholder', NULL);
    if (
      ($node = menu_get_object('node')) &&
      node_access('view', $node) &&
      !empty($node->field_header_image[LANGUAGE_NONE][0]['fid']) &&
      (empty($node->field_header_image[LANGUAGE_NONE][0]['is_default']) || !$node->status || $placeholder)
    ) {
      $path = file_create_url($node->field_header_image[LANGUAGE_NONE][0]['uri']);
      $inline_style = "background-image: url($path)";
      $output[] = [
        '#prefix' => '<div id="header-image" style="' . $inline_style . '"><div class="page-title"><h1 class="shadow-header">',
        '#markup' => hfcc_global_check_plain($node->title),
        '#suffix' => '</h1></div></div>',
      ];
    }
  }

}
