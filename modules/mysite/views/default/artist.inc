<?php

$view = new view();
$view->name = 'artist';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Artist';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Artist Info';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
$handler->display->display_options['row_options']['inline'] = array(
  'field_person_photo' => 'field_person_photo',
  'field_artist_bio' => 'field_artist_bio',
);
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['element_wrapper_type'] = 'h3';
$handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
/* Field: Content: Link to edit content */
$handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
$handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['label'] = '';
$handler->display->display_options['fields']['edit_node']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['edit_node']['element_wrapper_class'] = 'edit-link';
$handler->display->display_options['fields']['edit_node']['hide_empty'] = TRUE;
/* Field: Content: Photo */
$handler->display->display_options['fields']['field_person_photo']['id'] = 'field_person_photo';
$handler->display->display_options['fields']['field_person_photo']['table'] = 'field_data_field_person_photo';
$handler->display->display_options['fields']['field_person_photo']['field'] = 'field_person_photo';
$handler->display->display_options['fields']['field_person_photo']['label'] = '';
$handler->display->display_options['fields']['field_person_photo']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_person_photo']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_person_photo']['settings'] = array(
  'image_style' => 'large',
  'image_link' => 'file',
);
/* Field: Content: Bio */
$handler->display->display_options['fields']['field_artist_bio']['id'] = 'field_artist_bio';
$handler->display->display_options['fields']['field_artist_bio']['table'] = 'field_data_field_artist_bio';
$handler->display->display_options['fields']['field_artist_bio']['field'] = 'field_artist_bio';
$handler->display->display_options['fields']['field_artist_bio']['label'] = '';
$handler->display->display_options['fields']['field_artist_bio']['element_label_colon'] = FALSE;
/* Field: Content: Artist's Statement */
$handler->display->display_options['fields']['field_artist_statement']['id'] = 'field_artist_statement';
$handler->display->display_options['fields']['field_artist_statement']['table'] = 'field_data_field_artist_statement';
$handler->display->display_options['fields']['field_artist_statement']['field'] = 'field_artist_statement';
$handler->display->display_options['fields']['field_artist_statement']['alter']['text'] = '"[field_artist_statement]"';
$handler->display->display_options['fields']['field_artist_statement']['element_label_type'] = 'h4';
$handler->display->display_options['fields']['field_artist_statement']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_artist_statement']['hide_empty'] = TRUE;
/* Field: Content: Artist Website */
$handler->display->display_options['fields']['field_artist_website']['id'] = 'field_artist_website';
$handler->display->display_options['fields']['field_artist_website']['table'] = 'field_data_field_artist_website';
$handler->display->display_options['fields']['field_artist_website']['field'] = 'field_artist_website';
$handler->display->display_options['fields']['field_artist_website']['label'] = '';
$handler->display->display_options['fields']['field_artist_website']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_artist_website']['alter']['text'] = 'For more information on the artist, please visit [field_artist_website]';
$handler->display->display_options['fields']['field_artist_website']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_artist_website']['element_wrapper_type'] = 'strong';
$handler->display->display_options['fields']['field_artist_website']['click_sort_column'] = 'url';
/* Contextual filter: Content: Exhibition(s) (field_exhibition) */
$handler->display->display_options['arguments']['field_exhibition_target_id']['id'] = 'field_exhibition_target_id';
$handler->display->display_options['arguments']['field_exhibition_target_id']['table'] = 'field_data_field_exhibition';
$handler->display->display_options['arguments']['field_exhibition_target_id']['field'] = 'field_exhibition_target_id';
$handler->display->display_options['arguments']['field_exhibition_target_id']['default_action'] = 'default';
$handler->display->display_options['arguments']['field_exhibition_target_id']['default_argument_type'] = 'node';
$handler->display->display_options['arguments']['field_exhibition_target_id']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_exhibition_target_id']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_exhibition_target_id']['summary_options']['items_per_page'] = '25';
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'artist' => 'artist',
);
/* Filter criterion: Content: Bio (field_artist_bio) */
$handler->display->display_options['filters']['field_artist_bio_value']['id'] = 'field_artist_bio_value';
$handler->display->display_options['filters']['field_artist_bio_value']['table'] = 'field_data_field_artist_bio';
$handler->display->display_options['filters']['field_artist_bio_value']['field'] = 'field_artist_bio_value';
$handler->display->display_options['filters']['field_artist_bio_value']['operator'] = 'not empty';

/* Display: Exhibition EVA Field */
$handler = $view->new_display('entity_view', 'Exhibition EVA Field', 'entity_view_exhibition');
$handler->display->display_options['entity_type'] = 'node';
$handler->display->display_options['bundles'] = array(
  0 => 'exhibition',
);
$handler->display->display_options['show_title'] = 1;
