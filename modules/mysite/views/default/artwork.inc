<?php

$view = new view();
$view->name = 'artwork';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Artwork';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['style_plugin'] = 'grid';
$handler->display->display_options['style_options']['columns'] = '3';
$handler->display->display_options['row_plugin'] = 'fields';
$handler->display->display_options['row_options']['inline'] = array(
  'title' => 'title',
  'field_artist' => 'field_artist',
);
/* Relationship: Content: Image (field_artwork_image:fid) */
$handler->display->display_options['relationships']['field_artwork_image_fid']['id'] = 'field_artwork_image_fid';
$handler->display->display_options['relationships']['field_artwork_image_fid']['table'] = 'field_data_field_artwork_image';
$handler->display->display_options['relationships']['field_artwork_image_fid']['field'] = 'field_artwork_image_fid';
$handler->display->display_options['relationships']['field_artwork_image_fid']['label'] = 'image';
/* Relationship: Entity Reference: Referenced Entity */
$handler->display->display_options['relationships']['field_artist_target_id']['id'] = 'field_artist_target_id';
$handler->display->display_options['relationships']['field_artist_target_id']['table'] = 'field_data_field_artist';
$handler->display->display_options['relationships']['field_artist_target_id']['field'] = 'field_artist_target_id';
$handler->display->display_options['relationships']['field_artist_target_id']['label'] = 'Artist';
/* Field: File: Path */
$handler->display->display_options['fields']['uri']['id'] = 'uri';
$handler->display->display_options['fields']['uri']['table'] = 'file_managed';
$handler->display->display_options['fields']['uri']['field'] = 'uri';
$handler->display->display_options['fields']['uri']['relationship'] = 'field_image_fid';
$handler->display->display_options['fields']['uri']['label'] = '';
$handler->display->display_options['fields']['uri']['exclude'] = TRUE;
$handler->display->display_options['fields']['uri']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['uri']['file_download_path'] = TRUE;
/* Field: Content: Image */
$handler->display->display_options['fields']['field_artwork_image']['id'] = 'field_artwork_image';
$handler->display->display_options['fields']['field_artwork_image']['table'] = 'field_data_field_artwork_image';
$handler->display->display_options['fields']['field_artwork_image']['field'] = 'field_artwork_image';
$handler->display->display_options['fields']['field_artwork_image']['label'] = '';
$handler->display->display_options['fields']['field_artwork_image']['alter']['path'] = '[uri]';
$handler->display->display_options['fields']['field_artwork_image']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_artwork_image']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_artwork_image']['settings'] = array(
  'image_style' => 'extra_large',
  'image_link' => 'file',
);
/* Field: Content: Artist */
$handler->display->display_options['fields']['field_artist']['id'] = 'field_artist';
$handler->display->display_options['fields']['field_artist']['table'] = 'field_data_field_artist';
$handler->display->display_options['fields']['field_artist']['field'] = 'field_artist';
$handler->display->display_options['fields']['field_artist']['label'] = '';
$handler->display->display_options['fields']['field_artist']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_artist']['alter']['path'] = '[uri]';
$handler->display->display_options['fields']['field_artist']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_artist']['settings'] = array(
  'link' => 0,
);
/* Field: Content: Link to edit content */
$handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
$handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['label'] = '';
$handler->display->display_options['fields']['edit_node']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['edit_node']['element_wrapper_class'] = 'edit-link';
/* Sort criterion: Content: Last Name (or Professional Name) (field_last_name) */
$handler->display->display_options['sorts']['field_last_name_value']['id'] = 'field_last_name_value';
$handler->display->display_options['sorts']['field_last_name_value']['table'] = 'field_data_field_last_name';
$handler->display->display_options['sorts']['field_last_name_value']['field'] = 'field_last_name_value';
$handler->display->display_options['sorts']['field_last_name_value']['relationship'] = 'field_artist_target_id';
/* Contextual filter: Content: Exhibition(s) (field_exhibition) */
$handler->display->display_options['arguments']['field_exhibition_target_id']['id'] = 'field_exhibition_target_id';
$handler->display->display_options['arguments']['field_exhibition_target_id']['table'] = 'field_data_field_exhibition';
$handler->display->display_options['arguments']['field_exhibition_target_id']['field'] = 'field_exhibition_target_id';
$handler->display->display_options['arguments']['field_exhibition_target_id']['default_action'] = 'default';
$handler->display->display_options['arguments']['field_exhibition_target_id']['default_argument_type'] = 'node';
$handler->display->display_options['arguments']['field_exhibition_target_id']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_exhibition_target_id']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_exhibition_target_id']['summary_options']['items_per_page'] = '25';
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'artwork' => 'artwork',
);

/* Display: Exhibition EVA Field */
$handler = $view->new_display('entity_view', 'Exhibition EVA Field', 'entity_view_exhibition');
$handler->display->display_options['entity_type'] = 'node';
$handler->display->display_options['bundles'] = array(
  0 => 'exhibition',
);

/* Display: Artist EVA Field */
$handler = $view->new_display('entity_view', 'Artist EVA Field', 'entity_view_artist');
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: Content: Artist (field_artist) */
$handler->display->display_options['arguments']['field_artist_target_id']['id'] = 'field_artist_target_id';
$handler->display->display_options['arguments']['field_artist_target_id']['table'] = 'field_data_field_artist';
$handler->display->display_options['arguments']['field_artist_target_id']['field'] = 'field_artist_target_id';
$handler->display->display_options['arguments']['field_artist_target_id']['default_action'] = 'not found';
$handler->display->display_options['arguments']['field_artist_target_id']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['field_artist_target_id']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_artist_target_id']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_artist_target_id']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['field_artist_target_id']['validate']['type'] = 'node';
$handler->display->display_options['arguments']['field_artist_target_id']['validate_options']['types'] = array(
  'artist' => 'artist',
);
$handler->display->display_options['entity_type'] = 'node';
$handler->display->display_options['bundles'] = array(
  0 => 'artist',
);
