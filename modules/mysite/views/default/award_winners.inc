<?php

$view = new view();
$view->name = 'award_winners';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Award Winners';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Award Winners';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['style_plugin'] = 'grid';
$handler->display->display_options['style_options']['columns'] = '3';
$handler->display->display_options['row_plugin'] = 'fields';
$handler->display->display_options['row_options']['hide_empty'] = TRUE;
$handler->display->display_options['row_options']['default_field_elements'] = FALSE;
/* Field: Title (text for image link) */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['ui_name'] = 'Title (text for image link)';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['exclude'] = TRUE;
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_type'] = 'strong';
$handler->display->display_options['fields']['title']['element_class'] = 'no-link';
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
/* Field: Content: Path */
$handler->display->display_options['fields']['path']['id'] = 'path';
$handler->display->display_options['fields']['path']['table'] = 'node';
$handler->display->display_options['fields']['path']['field'] = 'path';
$handler->display->display_options['fields']['path']['label'] = '';
$handler->display->display_options['fields']['path']['exclude'] = TRUE;
$handler->display->display_options['fields']['path']['element_label_colon'] = FALSE;
/* Field: Content: Image */
$handler->display->display_options['fields']['field_artwork_image']['id'] = 'field_artwork_image';
$handler->display->display_options['fields']['field_artwork_image']['table'] = 'field_data_field_artwork_image';
$handler->display->display_options['fields']['field_artwork_image']['field'] = 'field_artwork_image';
$handler->display->display_options['fields']['field_artwork_image']['label'] = '';
$handler->display->display_options['fields']['field_artwork_image']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_artwork_image']['alter']['text'] = '<div class="highlight-link no-link"><a href="[path]">[title]</a></div>
[field_artwork_image]
<div class="hfc-overlay">
<div class="highlight-wrap">
</div>
</div>';
$handler->display->display_options['fields']['field_artwork_image']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_artwork_image']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_artwork_image']['settings'] = array(
  'image_style' => '600_x_440',
  'image_link' => '',
);
/* Field: Content: Title */
$handler->display->display_options['fields']['title_1']['id'] = 'title_1';
$handler->display->display_options['fields']['title_1']['table'] = 'node';
$handler->display->display_options['fields']['title_1']['field'] = 'title';
$handler->display->display_options['fields']['title_1']['label'] = '';
$handler->display->display_options['fields']['title_1']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title_1']['element_wrapper_type'] = 'strong';
$handler->display->display_options['fields']['title_1']['element_wrapper_class'] = 'no-link';
/* Field: Content: Artist */
$handler->display->display_options['fields']['field_artist']['id'] = 'field_artist';
$handler->display->display_options['fields']['field_artist']['table'] = 'field_data_field_artist';
$handler->display->display_options['fields']['field_artist']['field'] = 'field_artist';
$handler->display->display_options['fields']['field_artist']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_artist']['alter']['text'] = '[field_artist]<br>';
$handler->display->display_options['fields']['field_artist']['settings'] = array(
  'bypass_access' => 0,
  'link' => 0,
);
/* Field: Content: Award Place */
$handler->display->display_options['fields']['field_award_place']['id'] = 'field_award_place';
$handler->display->display_options['fields']['field_award_place']['table'] = 'field_data_field_award_place';
$handler->display->display_options['fields']['field_award_place']['field'] = 'field_award_place';
$handler->display->display_options['fields']['field_award_place']['label'] = '';
$handler->display->display_options['fields']['field_award_place']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_award_place']['alter']['text'] = '[field_award_place] - ';
$handler->display->display_options['fields']['field_award_place']['element_label_colon'] = FALSE;
/* Field: Content: Award */
$handler->display->display_options['fields']['field_award']['id'] = 'field_award';
$handler->display->display_options['fields']['field_award']['table'] = 'field_data_field_award';
$handler->display->display_options['fields']['field_award']['field'] = 'field_award';
$handler->display->display_options['fields']['field_award']['label'] = '';
$handler->display->display_options['fields']['field_award']['element_label_colon'] = FALSE;
/* Field: Content: Award Year */
$handler->display->display_options['fields']['field_award_year']['id'] = 'field_award_year';
$handler->display->display_options['fields']['field_award_year']['table'] = 'field_data_field_award_year';
$handler->display->display_options['fields']['field_award_year']['field'] = 'field_award_year';
$handler->display->display_options['fields']['field_award_year']['label'] = '';
$handler->display->display_options['fields']['field_award_year']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_award_year']['alter']['text'] = '<em>[field_award_year]</em>';
$handler->display->display_options['fields']['field_award_year']['element_type'] = 'div';
$handler->display->display_options['fields']['field_award_year']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_award_year']['settings'] = array(
  'format_type' => 'long',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
);
/* Sort criterion: Content: Award Year (field_award_year) */
$handler->display->display_options['sorts']['field_award_year_value']['id'] = 'field_award_year_value';
$handler->display->display_options['sorts']['field_award_year_value']['table'] = 'field_data_field_award_year';
$handler->display->display_options['sorts']['field_award_year_value']['field'] = 'field_award_year_value';
$handler->display->display_options['sorts']['field_award_year_value']['order'] = 'DESC';
/* Sort criterion: Content: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'awards' => 'awards',
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'award-winners';

/* Display: Exhibition EVA Field */
$handler = $view->new_display('entity_view', 'Exhibition EVA Field', 'entity_view_exhibition');
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: Content: Exhibition(s) (field_exhibition) */
$handler->display->display_options['arguments']['field_exhibition_target_id']['id'] = 'field_exhibition_target_id';
$handler->display->display_options['arguments']['field_exhibition_target_id']['table'] = 'field_data_field_exhibition';
$handler->display->display_options['arguments']['field_exhibition_target_id']['field'] = 'field_exhibition_target_id';
$handler->display->display_options['arguments']['field_exhibition_target_id']['default_action'] = 'default';
$handler->display->display_options['arguments']['field_exhibition_target_id']['default_argument_type'] = 'node';
$handler->display->display_options['arguments']['field_exhibition_target_id']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_exhibition_target_id']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_exhibition_target_id']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['entity_type'] = 'node';
$handler->display->display_options['bundles'] = array(
  0 => 'exhibition',
);
