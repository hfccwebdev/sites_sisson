<?php

$view = new view();
$view->name = 'exhibition_images';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Exhibition Images';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = '<none>';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['style_plugin'] = 'grid';
$handler->display->display_options['row_plugin'] = 'fields';
/* Field: Content: Exhibition Images */
$handler->display->display_options['fields']['field_exhibition_images']['id'] = 'field_exhibition_images';
$handler->display->display_options['fields']['field_exhibition_images']['table'] = 'field_data_field_exhibition_images';
$handler->display->display_options['fields']['field_exhibition_images']['field'] = 'field_exhibition_images';
$handler->display->display_options['fields']['field_exhibition_images']['label'] = '';
$handler->display->display_options['fields']['field_exhibition_images']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_exhibition_images']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_exhibition_images']['settings'] = array(
  'image_style' => 'large',
  'image_link' => 'file',
);
$handler->display->display_options['fields']['field_exhibition_images']['group_rows'] = FALSE;
$handler->display->display_options['fields']['field_exhibition_images']['delta_offset'] = '0';
/* Contextual filter: Content: Nid */
$handler->display->display_options['arguments']['nid']['id'] = 'nid';
$handler->display->display_options['arguments']['nid']['table'] = 'node';
$handler->display->display_options['arguments']['nid']['field'] = 'nid';
$handler->display->display_options['arguments']['nid']['default_action'] = 'default';
$handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
$handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = '1';
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'exhibition' => 'exhibition',
);

/* Display: Exhibition EVA Field */
$handler = $view->new_display('entity_view', 'Exhibition EVA Field', 'entity_view_exhibition');
$handler->display->display_options['entity_type'] = 'node';
$handler->display->display_options['bundles'] = array(
  0 => 'exhibition',
);
