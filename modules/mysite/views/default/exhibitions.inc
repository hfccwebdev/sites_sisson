<?php

$view = new view();
$view->name = 'exhibitions';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Exhibitions';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'The Sisson Art Gallery';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['style_plugin'] = 'grid';
$handler->display->display_options['style_options']['columns'] = '3';
$handler->display->display_options['row_plugin'] = 'fields';
$handler->display->display_options['row_options']['hide_empty'] = TRUE;
$handler->display->display_options['row_options']['default_field_elements'] = FALSE;
/* Field: Title (text for image link) */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['ui_name'] = 'Title (text for image link)';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['exclude'] = TRUE;
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
/* Field: Content: Subtitle */
$handler->display->display_options['fields']['field_exhibition_subtitle']['id'] = 'field_exhibition_subtitle';
$handler->display->display_options['fields']['field_exhibition_subtitle']['table'] = 'field_data_field_exhibition_subtitle';
$handler->display->display_options['fields']['field_exhibition_subtitle']['field'] = 'field_exhibition_subtitle';
$handler->display->display_options['fields']['field_exhibition_subtitle']['label'] = '';
$handler->display->display_options['fields']['field_exhibition_subtitle']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_exhibition_subtitle']['element_label_colon'] = FALSE;
/* Field: Content: Dates */
$handler->display->display_options['fields']['field_exhibition_dates']['id'] = 'field_exhibition_dates';
$handler->display->display_options['fields']['field_exhibition_dates']['table'] = 'field_data_field_exhibition_dates';
$handler->display->display_options['fields']['field_exhibition_dates']['field'] = 'field_exhibition_dates';
$handler->display->display_options['fields']['field_exhibition_dates']['label'] = '';
$handler->display->display_options['fields']['field_exhibition_dates']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_exhibition_dates']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_exhibition_dates']['settings'] = array(
  'format_type' => 'long',
  'custom_date_format' => '',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_remaining_days' => 0,
);
/* Field: Content: Path */
$handler->display->display_options['fields']['path']['id'] = 'path';
$handler->display->display_options['fields']['path']['table'] = 'node';
$handler->display->display_options['fields']['path']['field'] = 'path';
$handler->display->display_options['fields']['path']['label'] = '';
$handler->display->display_options['fields']['path']['exclude'] = TRUE;
$handler->display->display_options['fields']['path']['element_label_colon'] = FALSE;
/* Field: Content: Gallery Image */
$handler->display->display_options['fields']['field_gallery_image']['id'] = 'field_gallery_image';
$handler->display->display_options['fields']['field_gallery_image']['table'] = 'field_data_field_gallery_image';
$handler->display->display_options['fields']['field_gallery_image']['field'] = 'field_gallery_image';
$handler->display->display_options['fields']['field_gallery_image']['label'] = '';
$handler->display->display_options['fields']['field_gallery_image']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_gallery_image']['alter']['text'] = '<div class="highlight-link no-link"><a href="[path]">[title]</a></div>
[field_gallery_image]
<div class="hfc-overlay">
<div class="highlight-wrap">
</div>
</div>';
$handler->display->display_options['fields']['field_gallery_image']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_gallery_image']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_gallery_image']['settings'] = array(
  'image_style' => '600_x_440',
  'image_link' => '',
);
/* Field: Content: Title */
$handler->display->display_options['fields']['title_1']['id'] = 'title_1';
$handler->display->display_options['fields']['title_1']['table'] = 'node';
$handler->display->display_options['fields']['title_1']['field'] = 'title';
$handler->display->display_options['fields']['title_1']['label'] = '';
$handler->display->display_options['fields']['title_1']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title_1']['element_wrapper_type'] = 'div';
$handler->display->display_options['fields']['title_1']['element_wrapper_class'] = 'no-link';
/* Sort criterion: Content: Dates -  start date (field_exhibition_dates) */
$handler->display->display_options['sorts']['field_exhibition_dates_value']['id'] = 'field_exhibition_dates_value';
$handler->display->display_options['sorts']['field_exhibition_dates_value']['table'] = 'field_data_field_exhibition_dates';
$handler->display->display_options['sorts']['field_exhibition_dates_value']['field'] = 'field_exhibition_dates_value';
$handler->display->display_options['sorts']['field_exhibition_dates_value']['order'] = 'DESC';
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'exhibition' => 'exhibition',
);
$handler->display->display_options['filters']['type']['group'] = 1;

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'exhibitions';
$handler->display->display_options['menu']['type'] = 'normal';
$handler->display->display_options['menu']['title'] = 'Exhibitions';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['name'] = 'main-menu';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
